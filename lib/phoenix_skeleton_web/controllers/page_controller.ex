defmodule PhoenixSkeletonWeb.PageController do
  use PhoenixSkeletonWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
