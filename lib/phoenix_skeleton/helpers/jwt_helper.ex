defmodule JwtHelper do
  defmacro __using__(_) do
    quote do
      import Joken

      def createToken(data) do
        %{data: data}
          |> token
          |> with_validation("user_id", &(&1 == 1))
          |> with_signer(hs256("my_secret"))
          |> sign
          |> get_compact
      end

      def verifyToken(data) do
        %{data: data}
          |> token
          |> with_validation("user_id", &(&1 == 1))
          |> with_signer(hs256("my_secret"))
          |> sign
          |> get_compact
      end
    end
  end
end
