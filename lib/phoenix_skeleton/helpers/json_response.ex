defmodule JsonResponse do
  defmacro __using__(_) do
    quote do

      def jsonSuccess(conn, data) do
        conn
        |> json(%{data: data})
      end

      def jsonError(conn, data, status \\ 404) do
        conn
        |> put_status(status)
        |> json(%{errors: data})
      end
    end
  end
end
